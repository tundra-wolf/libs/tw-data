/// Floating point literals
#[derive(Debug)]
pub enum FloatingPointLiteral {
    Float32(f32),
    Float64(f64),
}
