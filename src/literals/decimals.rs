use crate::types::{Decimal32, Decimal64};

/// Decimal literals
#[derive(Debug)]
pub enum DecimalLiteral {
    Decimal32(Decimal32),
    Decimal64(Decimal64),
}
