use crate::types::{AsciiString, String16, String32};

/// String literals. The following types are supported:
///
/// * Ascii - 8-bit character string
/// * AsciiC - 8-bit character string, zero terminated (C style)
/// * Utf8 - UTF-8 character string
#[derive(Debug)]
pub enum StringLiteral {
    Ascii(AsciiString),
    Utf8(String),
    Utf16(String16),
    Utf32(String32),
}
