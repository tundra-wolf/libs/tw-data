use crate::types::characters::{Char16, Char32};

/// Character literals
#[derive(Debug)]
pub enum CharacterLiteral {
    Ascii(u8),
    Utf8(char),
    Utf16(Char16),
    Utf32(Char32),
}
