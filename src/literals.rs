pub mod characters;
pub mod dates;
pub mod decimals;
pub mod floating_points;
pub mod integers;
pub mod strings;

pub use crate::literals::characters::CharacterLiteral;
pub use crate::literals::dates::DateLiteral;
pub use crate::literals::decimals::DecimalLiteral;
pub use crate::literals::floating_points::FloatingPointLiteral;
pub use crate::literals::integers::IntegerLiteral;
pub use crate::literals::strings::StringLiteral;
