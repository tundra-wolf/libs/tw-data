/// UTF-16 character literal
#[derive(Clone, Copy, Debug)]
pub struct Char16(pub u16, pub u16);

/// UTF-32 character literal
#[derive(Clone, Copy, Debug)]
pub struct Char32(pub u32);
