/// Decimal with 32-bit value, stored as a value and a divisor.
#[derive(Clone, Copy, Debug)]
pub struct Decimal32(i32, i32);

/// Decimal with 64-bit value, stored as value and a divisor.
#[derive(Clone, Copy, Debug)]
pub struct Decimal64(i64, i64);
