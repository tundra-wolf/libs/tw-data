use crate::types::characters::Char16;

/// Value at which the first 16-bit word indicates a second one is needed
const UTF16_FIRST_U16: u16 = 0xD800;

/// A UTF-16 string
#[derive(Clone, Debug)]
pub struct String16 {
    data: Vec<u16>,
}

impl String16 {
    pub fn new() -> String16 {
        String16 { data: Vec::new() }
    }

    pub fn push(&mut self, ch: Char16) {
        self.data.push(ch.0);
        if ch.0 >= UTF16_FIRST_U16 {
            self.data.push(ch.1);
        }
    }

    pub fn push_str(&mut self, string: &[Char16]) {
        for ch in string {
            self.push(*ch);
        }
    }

    pub fn as_slice(&self) -> &[u16] {
        &self.data
    }
}

impl From<&[Char16]> for String16 {
    fn from(value: &[Char16]) -> String16 {
        let mut s = String16::new();
        s.push_str(value);

        s
    }
}

impl From<&[u16]> for String16 {
    fn from(value: &[u16]) -> String16 {
        let mut s = String16::new();
        s.data = value.to_vec();

        s
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn string16_as_slice() {
        let s =
            String16::from([Char16(0xD7FF, 0x0000), Char16(0xD800, 0x0000)].as_slice());
        assert_eq!(s.as_slice(), &[0xD7ff, 0xD800, 0x0000]);
    }

    #[test]
    fn string16_new() {
        let s = String16::new();
        assert_eq!(s.data, vec![]);
    }

    #[test]
    fn string16_from() {
        let s =
            String16::from([Char16(0xD7FF, 0x0000), Char16(0xD800, 0x0000)].as_slice());
        assert_eq!(s.data, vec![0xD7ff, 0xD800, 0x0000]);

        let s = String16::from([0xD7ff, 0xD800, 0x0000].as_slice());
        assert_eq!(s.data, vec![0xD7ff, 0xD800, 0x0000]);
    }

    #[test]
    fn string16_push() {
        let mut s = String16::new();

        s.push(Char16(0xD7ff, 0x0000));
        assert_eq!(s.data, vec![0xD7ff]);

        s.push(Char16(0xD800, 0x0000));
        assert_eq!(s.data, vec![0xD7ff, 0xD800, 0x0000]);
    }

    #[test]
    fn string16_push_str() {
        let mut s = String16::new();

        let v = &vec![Char16(0xD7FF, 0x0000), Char16(0xD800, 0x0000)];
        s.push_str(v);
        assert_eq!(s.data, vec![0xD7ff, 0xD800, 0x0000]);

        let v = &vec![
            Char16(0xD7FF, 0x0000),
            Char16(0xD800, 0x0000),
            Char16(0x1800, 0x0000),
        ];
        s.push_str(v);
        assert_eq!(s.data, vec![0xD7ff, 0xD800, 0x0000, 0xD7FF, 0xD800, 0x0000, 0x1800]);
    }
}
