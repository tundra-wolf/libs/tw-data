use crate::types::characters::Char32;

/// A UTF-32 string
#[derive(Clone, Debug)]
pub struct String32 {
    data: Vec<u32>,
}

impl String32 {
    pub fn new() -> String32 {
        String32 { data: Vec::new() }
    }

    pub fn push(&mut self, ch: Char32) {
        self.data.push(ch.0);
    }

    pub fn push_str(&mut self, string: &[Char32]) {
        for ch in string {
            self.data.push(ch.0);
        }
    }

    pub fn as_slice(&self) -> &[u32] {
        &self.data
    }
}

impl From<&[Char32]> for String32 {
    fn from(value: &[Char32]) -> String32 {
        let mut s = String32::new();
        s.push_str(value);

        s
    }
}

impl From<&[u32]> for String32 {
    fn from(value: &[u32]) -> String32 {
        let mut s = String32::new();
        s.data = value.to_vec();

        s
    }
}

#[cfg(test)]
pub mod tests {
    use super::*;

    #[test]
    fn string32_as_slice() {
        let s = String32::from([Char32(0xffffffff), Char32(0xabcdffff)].as_slice());
        assert_eq!(s.as_slice(), &[0xffffffff, 0xabcdffff]);
    }

    #[test]
    fn string32_new() {
        let s = String32::new();
        assert_eq!(s.data, vec![]);
    }

    #[test]
    fn string32_from() {
        let s = String32::from([Char32(0xffffffff), Char32(0xabcdffff)].as_slice());
        assert_eq!(s.data, vec![0xffffffff, 0xabcdffff]);

        let s = String32::from([0xffffffff, 0xfedcffff].as_slice());
        assert_eq!(s.data, vec![0xffffffff, 0xfedcffff]);
    }

    #[test]
    fn string32_push() {
        let mut s = String32::new();

        s.push(Char32(0xffff0123));
        assert_eq!(s.data, vec![0xffff0123]);

        s.push(Char32(0x0def1234));
        assert_eq!(s.data, vec![0xffff0123, 0x0def1234]);
    }

    #[test]
    fn string32_push_str() {
        let mut s = String32::new();

        let v = &vec![Char32(0xffffffff), Char32(0x0abcfedc), Char32(0x0defabcd)];
        s.push_str(v);
        assert_eq!(s.data, vec![0xffffffff, 0x0abcfedc, 0x0defabcd]);

        let v = &vec![Char32(0x12345678), Char32(0x5676789a)];
        s.push_str(v);
        assert_eq!(
            s.data,
            vec![0xffffffff, 0x0abcfedc, 0x0defabcd, 0x12345678, 0x5676789a]
        );
    }
}
