/// ASCII Code page options
#[derive(Clone, Copy, Debug)]
pub enum AsciiCodePage {
    Ascii7Bit367,
    IbmPc437,
    WindowsWestern1252,
}

impl Default for AsciiCodePage {
    fn default() -> Self {
        Self::IbmPc437
    }
}

impl From<u16> for AsciiCodePage {
    fn from(value: u16) -> Self {
        match value {
            367 => Self::Ascii7Bit367,
            437 => Self::IbmPc437,
            1252 => Self::WindowsWestern1252,
            _ => Self::IbmPc437,
        }
    }
}
