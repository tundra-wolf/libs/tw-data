pub mod encoding;

use crate::types::strings::ascii_string::encoding::AsciiCodePage;

/// Stores an ASCII string
#[derive(Clone, Debug)]
pub struct AsciiString {
    code_page: AsciiCodePage,
    data: Vec<u8>,
}

impl AsciiString {
    pub fn new() -> AsciiString {
        AsciiString { code_page: AsciiCodePage::IbmPc437, data: Vec::new() }
    }
}
